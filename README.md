# 		RSS Feed

This is my (Nika Shelia) Python task for the RSS feed project.
Please provide tips on how to improve the code.


# Installation

rss_reader can be used both through directly executing "\__main__.py" from python or 
installing it by running install .sh file (Windows)

on Linux you need to enter the directory of the file (where setup.py is located) and type "pip3 install -e ."


# Usage:



## If you have installed this program:

you can directly run it through the console by typing "nika_rss_reader"
example: >nika_rss_reader -h

## If you have not / do not intend to install this program through install.shell file

You can execute it through cmd using python + /path/to/file/rss_reader/\__\_main____.py
example: >python "C:\Users\user\desktop\Folder\rss_reader\_\_main_\_\_.py" -h    


# Examples:

--
Display the feed in a JSON format:
nika_rss_reader "https://news.yahoo.com/rss/" -json 


--
Only display a specific amount of items:

nika_rss_reader "https://news.yahoo.com/rss/" -limit 1

Add -h argument to view all available commands.
--