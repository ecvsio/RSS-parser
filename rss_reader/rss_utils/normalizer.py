import re


def remove_between(str_input, slice_start, slice_end):
    """

    Input:
    str_input - string to parse
    slice_start - start of substring to remove
    slice_end - end of substring to remove

    Output:
    str2 - parsed string

    Used in normalize_html
    Removes substrings from given string between slice_start and slice_end

    """
    str2 = re.sub(f'{slice_start}.*?{slice_end}', '', str_input, flags=re.DOTALL)
    return str2


def normalize_html(x,json=False):
    """

    Cleans out the data, so it does not contain any non-user friendly text.

    Input:
    x
    Output:
    y

    """
    y = remove_between(x, "<img src", "/>")
    y = remove_between(y, "<a", ">")
    y = y.replace("<em>", "")
    y = y.replace("</em>", "")
    y = y.replace("</a>", "")
    y = y.replace("</p>", "")
    y = y.replace("<p>", "")
    y = y.replace("…", "... ")
    if json:
        y = y.replace("<br>", "")
    else:
        y = y.replace("<br>", "\n")
    y = y.replace("Read more...", "")

    return y
