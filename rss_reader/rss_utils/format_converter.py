from json2html import *

import re

from xhtml2pdf import pisa

filename = "rss_feed"

try:
    from rss_utils.mylogger import *
except Exception as exc:
    from .mylogger import *


def generate_html(verbosity, json_input):
    log(verbosity, "", "", "")
    html_format = json2html.convert(json=json_input)
    html_format = html_format.replace("<li>", "")
    html_format = html_format.replace("</td>", "</td>\n")
    html_format = re.sub(r'(https://)(.+)(</td>)', r'<a href="\1\2"> \2</a>\3', html_format)
    html_format = html_format.replace("""<table border="1">""",
                                      """<table border="1"><body style="background-color:#1f1e26;">""")
    html_format = html_format.replace("</li>", "")
    html_format = html_format.replace("<td>",
                                      """<td style="background-color:#2f2e3b;color:FFF3FF;padding: 1px;width: 120%">""")
    html_format = html_format.replace("<th>",
                                      """<th style="background-color:#3e3c4d;color:FFFFFF;padding: 1px;width: 120%">""")
    html_format = html_format.replace("</table>", "<br>")
    return html_format


def create_pdf_file(verbosity, location, json_input):
    try:
        log(verbosity, "", "", f"Saving PDF file to {location}")
        html_data = generate_html(json_input)
        # open output file for writing (truncated binary)
        result_file = open(f"{location}/{filename}.pdf", "w+b")
        # convert HTML to PDF
        pisa_status = pisa.CreatePDF(
            html_data,  # the HTML to convert
            dest=result_file)  # file handle to recieve result

        # close output file
        result_file.close()  # close output file

        # return False on success and True on errors
        print("PDF File created")
    except FileNotFoundError as exc:
        log(verbosity, "Location not found", f"Location not found {exc}")


def create_html_file(verbosity, location, json_input):
    try:
        log(verbosity, "", "", f"Saving HTML file to {location}")
        html_format = json2html.convert(json=json_input)
        html_format = html_format.replace("<li>", "")
        html_format = html_format.replace("</td>", "</td>\n")
        html_format = re.sub(r'(https://)(.+)(</td>)', r'<a href="\1\2"> \2</a>\3', html_format)
        html_format = html_format.replace("""<table border="1">""",
                                          """<table border="1"><body style="background-color:#1f1e26;">""")
        html_format = html_format.replace("</li>", "")
        html_format = html_format.replace("<td>", """<td style="background-color:#2f2e3b;color:FFF3FF">""")
        html_format = html_format.replace("<th>", """<th style="background-color:#3e3c4d;color:FFFFFF">""")
        html_format = html_format.replace("</table>", "<br>")
        file = open(f"{location}/{filename}.html", "w")
        file.write(html_format)
        file.close()
        print("HTML File created")
    except FileNotFoundError:
        log(verbosity, "Location not found")
