from colorama import Fore, init, Style
import os
import datetime
import inspect

color = Fore

end_color = Style.RESET_ALL
opt_nocolor = Style.RESET_ALL


def check_color(colorize_input):
    global opt_nocolor
    if colorize_input:
        opt_nocolor = ""


if os.name == 'nt':
    init(convert=True)
else:
    init(convert=False)


# LOG
def log(verbosity, *prompts):
    """

    Prints out the current date/time and a specific log depending on the verbosity.
    example usage:

    log(1,"error occurred","error, can not parse data","error, can not parse data due to type mismatch")
    This will output the first error - "error occurred", since the verbosity is set to 1

    log(1,"","","error, can not parse data due to type mismatch")
    We can also do this, which will not output anything unless verbosity is at least 3

    Current format I've set is ERROR - WARNING - INFO - DEBUG
    Allowed more than 4 arguments in case levels are required later on.

    Higher means more verbose, 0 means no log messages, 1 means only ERROR, 2 WARNING, etc.


    """
    if verbosity:
        verbosity = int(verbosity)
    else:
        verbosity = 0

    now = datetime.datetime.now().strftime("%y-%m-%d %H:%M:%S")
    curr_time = now
    prompts_length = len(prompts)
    original_verbosity = verbosity
    logcolor = color.LIGHTCYAN_EX

    if verbosity > prompts_length:
        verbosity = prompts_length

    if verbosity == 1:
        logcolor = color.RED
    elif verbosity == 2:
        logcolor = color.RED
    elif verbosity == 4:
        logcolor = color.LIGHTYELLOW_EX

    if verbosity != 0 and prompts[verbosity - 1]:
        caller_frame = inspect.stack()[1]
        caller_filename_full = caller_frame.filename
        caller_filename_only = os.path.splitext(os.path.basename(caller_filename_full))[0]
        filename_info = ""
        if original_verbosity > 6:
            filename_info = f"| {caller_filename_only} |"
        print(f"\n{logcolor}{opt_nocolor}>>>> | {curr_time} {filename_info}"
              f" | LOG MESSAGE |: {prompts[verbosity - 1]}\n{end_color}")


def custom_print(color_to_print, text, newline=True):
    printcolor = getattr(color, color_to_print)
    if newline:
        print(f"{printcolor}{opt_nocolor}{text}{end_color}")
    else:
        print(f"{printcolor}{opt_nocolor}{text}{end_color}", end="")
