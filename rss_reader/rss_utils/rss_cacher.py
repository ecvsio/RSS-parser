from collections import OrderedDict

import dateparser

import sqlite3

import urllib3

import xmltodict

try:
    from rss_utils.config import *
    from rss_utils.mylogger import *
except Exception as exc:
    from .config import *
    from .mylogger import *

dbname = "rss_cache.db"

table_format = list_of_outputs + ["rss"]

# Create the database
def create_db(verbosity):
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    table_create_format = ""
    for item in table_format:
        if item == table_format[0]:
            table_create_format = table_create_format + f"{item}"
        else:
            table_create_format = table_create_format + f",{item}"
    c.execute(
        f"""
    CREATE TABLE if not exists cache
            ({table_create_format})
    """)
    conn.commit()
    conn.close()


# delete later, don't need
# "SELECT * FROM cache WHERE Title = 'a'"
def query_db(verbosity, query):
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    c.execute(query)
    queried_data = c.fetchall()
    conn.close()
    return queried_data


def time_gmt_format(verbosity, str_datetime):
    date_time_obj = dateparser.parse(str_datetime, date_formats=['%d/%m/%Y, %H:%M %Z'],
                                     settings={'TO_TIMEZONE': 'GMT'})

    return date_time_obj.strftime('%Y%m%d')


# add the rss title as a field
# receives data as xml_data["rss"]["channel"]
def get_items_from_data(verbosity, rss_data, rss_feed_url):
    xml_data_items = rss_data["item"]
    xml_data_rss_title = rss_feed_url
    item_dictionary = []
    for item in xml_data_items:
        temp_dict = {}
        for item_value in item:
            if item_value in list_of_outputs:
                temp_dict[item_value] = item[item_value]
                if item_value == "pubDate":
                    temp_dict[item_value] = time_gmt_format(verbosity, item[item_value])
        temp_dict["rss"] = xml_data_rss_title
        item_dictionary.append(temp_dict)
    return item_dictionary


def is_item_in_database(verbosity, title, rss):
    log(verbosity, "", "", "", "", "", "",
        f"Checking if any item with the title ({title}) and url ({rss}) is in the database")
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    c.execute("""SELECT * FROM cache WHERE Title = ? AND rss = ?""",
              (title, rss))
    queried_data = c.fetchone()
    conn.close()
    if queried_data:
        log(verbosity, "", "", "", "", "", "", "Item was found in the database")
        return True
    else:
        log(verbosity, "", "", "", "", "", "", "Item was not found in the database")
        return False


# Input is list [a,b,c,d]
def insert_to_db(verbosity, data_to_insert):
    conn = sqlite3.connect(dbname)
    c = conn.cursor()
    insert_format = ""
    for item in data_to_insert:
        if item == data_to_insert[0]:
            insert_format = insert_format + f"?"
        else:
            insert_format = insert_format + f", ?"
    data_to_insert = tuple(data_to_insert)
    rss_index = table_format.index("rss")
    title_index = table_format.index("title")
    if not is_item_in_database(verbosity, data_to_insert[title_index], data_to_insert[rss_index]):
        log(verbosity, "", "", "", f"Inserting items to database", f"Inserting to database - {data_to_insert}")
        c.execute(f"INSERT INTO cache VALUES ({insert_format})", data_to_insert)
    else:
        pass
        # log placeholder
    conn.commit()
    conn.close()


# requires xml_data["rss"]["channel"]
def start_cache(verbosity, rss_feed_data_input, rss_feed_url):
    create_db(verbosity)
    log("", "", "Caching items", f"Caching items from {rss_feed_url}")
    itemlist = get_items_from_data(verbosity, rss_feed_data_input, rss_feed_url)
    for item in itemlist:
        data_to_insert = []
        for value in table_format:
            try:
                data_to_insert.append(item[value])
            except KeyError:
                data_to_insert.append("")
        insert_to_db(verbosity, data_to_insert)


def get_cached_items(verbosity, date, rss_url=""):
    try:
        log(verbosity, "", "", "Attempting to retrieve catched items", "Attempting to retrieve catched items Using the "
                                                                       f"query: {date} | {rss_url}")
        conn = sqlite3.connect(dbname)
        c = conn.cursor()
        if rss_url:
            c.execute("""SELECT * FROM cache WHERE pubDate = ? AND rss = ?""",
                      (date, rss_url))
        else:
            c.execute("""SELECT * FROM cache WHERE pubDate = ?""",
                      (date,))
        queried_data = c.fetchall()
        conn.close()
    except Exception as e:
        log(verbosity, "Local cache not found", f"Database not found",
            f"{dbname} not found, There might not be any cached items to retrieve", f"{dbname} not found, There might "
                                                                                    f"not be any cached items to "
                                                                                    f"retrieve - {e}")
    return queried_data


def inject_cached(verbosity, cached_items, xml_data_to_replace, rss_feed_url=""):
    log(verbosity, "", "", "", "Injecting cached items into the xml format")
    y = ""
    m = ""
    d = ""
    new_item_list = []
    for cached_item in cached_items:
        temp_ord_dict = OrderedDict([])
        value_index = 0
        for value in cached_item:
            key = table_format[value_index]
            if key == "pubDate":
                y = value[0:4]
                m = value[4:6]
                d = value[6:8]
                normalized_date = f"{y}-{m}-{d}"
                temp_ord_dict.update({key: normalized_date})
            else:
                temp_ord_dict.update({key: value})
            value_index += 1

        new_item_list.append(temp_ord_dict)
    xml_data_to_replace["item"] = new_item_list
    if rss_feed_url:
        xml_data_to_replace["title"] = f"Cached data from {rss_feed_url}"
    else:
        xml_data_to_replace["title"] = "Cached data"
    xml_data_to_replace["description"] = f"{y}-{m}-{d}"
    log(verbosity, "", "", "", "Injection complete")
    return xml_data_to_replace
