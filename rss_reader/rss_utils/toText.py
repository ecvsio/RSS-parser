"""

start -> normalize_data -> normalize_html -> custom_print

start begins by accessing the needed data and transferring it to normalize_data function
The normalize_data then alters the data depending on what field it is and makes it user-friendly, it then transfers it
to normalize_html and afterwards it prints the data out in console using custom_print from the logger module
the custom_print decides whether the print should be colored.

"listofoutputs" variable decides what outputs will be given and in what order.


"""
try:
    from rss_utils.config import list_of_outputs
    from rss_utils.mylogger import log
    from rss_utils.mylogger import custom_print
    from rss_utils.normalizer import normalize_html
except Exception as exc:
    from .config import list_of_outputs
    from .mylogger import log
    from .mylogger import custom_print
    from .normalizer import normalize_html


def normalize_data(data_type, data_to_print):
    """
    Normalizes the data and prints it, makes it more user-friendly and easier to read

    inputs:
    data_type - the type of data to be displayed (title, description, etc.)
    data_to_print - the actual data to be displayed
    """

    if data_type.lower() == "pubdate":
        name_of_field = "Publication Date"
        if "GMT" not in data_to_print:
            data_to_print = data_to_print.replace("T", " ")
        data_to_print = data_to_print.replace("Z", "")
    else:
        name_of_field = data_type.capitalize()
    data_to_print_normalized = normalize_html(data_to_print)

    custom_print("LIGHTRED_EX", f"{name_of_field}: ", newline=False)
    if name_of_field.lower() == "link":
        custom_print("LIGHTBLUE_EX", f"{data_to_print_normalized}")
    else:
        custom_print("WHITE", f"{data_to_print_normalized}")


def start(main_data, limit=100, verbosity=0):
    """

    Begins parsing the rss data, passing the required data to the "normalize_data" function

    inputs:

    data - rss data to parse
    limit - amount of items to display (default is 100)
    verbosity - verbosity of the code (default is 0) (higher means more verbose)

    """

    separator_color = "LIGHTWHITE_EX"
    separator = f"\n################################################################################################\n"

    # Checks if the requested limit input is higher than the length of the whole feed
    if limit > len(main_data["item"]):
        limit = len(main_data["item"]) - 1
    # Iterates through the feed up to the given limit value, checking if the field is found
    # if the field is not found, it simply skips over to the next field.
    # if the field is found, sends all the data to the normalize_data function, which will make it more user-friendly
    if verbosity:
        log(verbosity, "", "", f"Beginning data output:")
    custom_print(separator_color, separator)

    # <editor-fold desc="  Printing the title  ">
    try:
        log(verbosity, "", "", "Printing RSS title")
        custom_print("YELLOW", f"""{main_data["title"]}\n""")
        custom_print("YELLOW", f"""{main_data["description"]}""")
    except KeyError:
        log(verbosity, "", "RSS Title not found", "")
    except Exception as e:
        log(verbosity, "Unknown error", f"Error:\n {e}")
    # </editor-fold>

    #######################
    custom_print(separator_color, separator)
    ########################

    # <editor-fold desc="  Printing the body  ">
    for x in range(1, limit + 1):
        log(verbosity, "", "", f"Printing item #{x}\n")
        for output in list_of_outputs:
            try:
                data_to_print = main_data["item"][x][output]
                type_of_data = output
                normalize_data(type_of_data, data_to_print)
            except KeyError:
                log(verbosity, "", "", f"Could not print {output}, value not found.")
            except Exception as e:
                log(verbosity, "Unexpected error", f"Error: {Exception}", f"Error - {Exception}:\n {e}")
        custom_print(separator_color, separator)
