"""

start -> dict_cleaner --> normalize --> normalize_html

start begins by accessing the needed data and calling dict_cleaner function
dict_cleaner then removes all the unneeded fields ("listofoutputs" decides what fields will not be deleted.)
Afterwards the dict_cleaner function returns filtered data, and it's passed down to the normalize function it then
transfers it to normalize_html and custom_print from the logger module
the custom_print decides whether the print should be colored.

"""
try:
    from rss_utils.config import list_of_outputs
    from rss_utils.format_converter import create_pdf_file
    from rss_utils.format_converter import create_html_file
    from rss_utils.mylogger import log
    from rss_utils.normalizer import normalize_html
except Exception as exc:
    from .config import list_of_outputs
    from .format_converter import create_pdf_file
    from .format_converter import create_html_file
    from .mylogger import log
    from .normalizer import normalize_html

import copy

import json


def start(data, limit=100, verbosity=0, destination=""):
    """
    Begins parsing the rss data, passing the required data to the "dict_cleaner" function, which parses the data and
    passes it back, then the data is sent to the "normalize" function, which normalizes the data and prints it out


    inputs:

    data - rss data to parse
    limit - amount of items to display (default is 100)
    destination - Whether or not this module is being used for creating a document and if so, where to save it.
    verbosity - verbosity of the code (default is 0) (higher means more verbose)

    """

    # Defining variables we will need later
    item_data = data["item"]
    main_data = data

    # Calls the function and stores the cleaned / parsed data in output_dict variable
    output_dict = dict_cleaner(item_data)
    # Adds a title and a description

    rss_title = main_data["title"]
    rss_description = main_data["description"]
    output_dict.insert(0, {"Title": f"{rss_title}", "Description": f"{rss_description}"})

    # Converts the data into a JSON file and prints out the result
    result = json.dumps(output_dict[0:limit + 1], indent=1, ensure_ascii=False)
    log(verbosity, "", "", "Json file created")
    normalize(verbosity, result, destination=destination)


def dict_cleaner(dict_old, destination=""):
    """

    Function to parse the dictionary and remove all unnecessary fields, looks at each field and checks if it matches
    list_of_outputs.

    inputs:
    dict_old - Dictionary to parse
    destination - Whether or not this module is being used for creating a document and if so, where to save it.
    outputs:
    dict_new - Result

    """
    dict_new = copy.deepcopy(dict_old)
    for x in range(0, len(dict_old)):
        for y in dict_old[x]:  # y is item
            if y not in list_of_outputs:
                dict_new[x].pop(y)
    return dict_new


def normalize(verbosity, data, destination=""):
    """

    Normalizes data, passing it in the normalize module using the normalize_html function,

    inputs:
    verbosity - for logging
    data - the data to normalize
    destination - Whether or not this module is being used for creating a document and if so, where to save it.


    """
    log(verbosity, "", "", "Normalizing data")
    data = normalize_html(data, json=True)
    log(verbosity, "", "", "Data normalization complete")
    if destination[-4:] == "html":
        create_html_file(verbosity, destination[0:-5], data)
    elif destination[-3:] == "pdf":
        create_pdf_file(verbosity, destination[0:-4], data)
    else:
        log(verbosity, "", "", "Printing the data in JSON format")
        print(data)
        log(verbosity, "", "", "Printing complete")
