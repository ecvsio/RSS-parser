try:
    from rss_utils.mylogger import log
    from rss_utils.mylogger import Style
    from rss_utils.mylogger import check_color
    from rss_utils.rss_cacher import *

    from rss_utils import toJson
    from rss_utils import toText
    from rss_utils import toText

except Exception as exc:
    from .mylogger import log
    from .mylogger import Style
    from .mylogger import check_color

    from . import toJson
    from . import toText
    from .rss_cacher import *

import urllib3

from collections import OrderedDict

import xmltodict

"""

start -> get_rss_data --> start

start begins by using the get_rss_data function to request data from the rss feed.
then checks for arguments to figure out which module should be called.
this module also uses the colorize argument to decide whether or not the text will be colorized

"""

dummy_data = OrderedDict(
    [('title', 'template'), ('link', 'template'), ('description', 'template'), ('language', 'en-US'),
     ('copyright', 'template'), ('pubDate', 'template'), ('ttl', '5'),
     ('image', OrderedDict([('title', 'template'), ('link', 'template'), ('url', '')])), ('item', []),
     ('media:credit', OrderedDict([('@role', 'template')]))])

opt_nocolor = Style.RESET_ALL


def get_rss_data(rss_url, verbosity):
    """

    Function to request data from the RSS feed

    Input:
    rss_url - RSS Feed url
    verbosity - verbosity of the code (default is 0) (higher means more verbose)
    --
    Output: (data) Ordered Dictionary of xml values inside the RSS feed

    """
    http = urllib3.PoolManager()
    response = http.request('GET', rss_url)
    log(verbosity, "", "", f"Attempting to get the xml data", f"Attempting to get the xml data from {rss_url}")
    data = xmltodict.parse(response.data)
    return data


def start(url, limit=100, verbosity=1, json=False, colorize=False, date="", destination=""):
    """
    Function to decide what to do with the RSS feed data.

    inputs:
    url - RSS Feed url
    limit - the amount of items to display (default is 100)
    verbosity - verbosity of the code (default is 0) (higher means more verbose)
    json - should output be a json format (default is false)

    """
    check_color(colorize)
    main_data = ""
    if url and not date:
        data = get_rss_data(url, verbosity)
        main_data = data["rss"]["channel"]
        start_cache(verbosity, data["rss"]["channel"], url)
    if date:
        main_data = dummy_data
        cached_items = get_cached_items(verbosity, date, rss_url=url)
        main_data = inject_cached(verbosity, cached_items, main_data, rss_feed_url=url)
    if json:
        toJson.start(main_data, limit=limit, verbosity=verbosity)
    elif toText:
        toText.start(main_data, limit=limit, verbosity=verbosity)
    if destination and not date:
        toJson.start(main_data, limit=limit, verbosity=verbosity, destination=destination)
    if destination and date:
        toJson.start(main_data, limit=limit, verbosity=verbosity, destination=destination)
