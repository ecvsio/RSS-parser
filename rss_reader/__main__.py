"""

This is the main piece of code, which takes in all the arguments and passes it to necessary modules.
Gets the version number from miscdata

"""

import argparse

try:
    from rss_utils import getdata
    from rss_utils import mylogger
    from rss_utils.config import *
except Exception as exc:
    from .rss_utils import getdata
    from .rss_utils import mylogger
    from .rss_utils.config import *

parser = argparse.ArgumentParser()
group = parser.add_mutually_exclusive_group()
group2 = parser.add_mutually_exclusive_group()

group.add_argument("--version", help="Displays the version ", action="store_true")

group.add_argument('url', nargs='?', default="")

parser.add_argument("--limit", "-l", help="Maximum items to display (default is 100)", default=100, type=int)

parser.add_argument("--date", "-d", help="Maximum items to display (default is 100)", default="", type=str)

parser.add_argument("--verbosity", "-v", help="Set the output verbosity (default is 0)", default=1, type=int)

parser.add_argument("--json", "-j", help="Output into json format (default is false)", action="store_true")

group2.add_argument("--topdf", help="Converts to PDF and saves to target location", default="", type=str)

group2.add_argument("--tohtml", help="Converts to HTML and saves to target location", default="", type=str)

parser.add_argument("--colorize", "-c", help="Colorize the output to make it easier to read", action="store_true")

args = parser.parse_args()

color_option = True


def main():
    destination = ""
    if args.tohtml:
        destination = fr"{args.tohtml}.html"
    if args.topdf:
        destination = fr"{args.topdf}.pdf"
    if args.date:
        try:
            getdata.start("",
                          limit=args.limit,
                          verbosity=args.verbosity,
                          json=args.json,
                          colorize=args.colorize,
                          date=args.date,
                          destination=destination
                          )
        except Exception as e:
            mylogger.log(args.verbosity,
                         "",
                         "",
                         "",
                         f"{e}")
    if not args.url and not args.version and not args.date:
        parser.print_help()
    if args.version:
        print(f"\nCurrent version - {version}")
    if args.url and not args.date:
        try:
            getdata.start(args.url,
                          limit=args.limit,
                          verbosity=args.verbosity,
                          json=args.json,
                          colorize=args.colorize,
                          date=args.date,
                          destination=destination
                          )
        except Exception as e:
            mylogger.log(args.verbosity,
                         "An error has occurred, Please check if the provided URL is valid",
                         f"An error has occurred, Please check if the provided URL:\n ({args.url}) is valid",
                         f"An error has occurred, Please check if the provided URL:\n ({args.url}) is valid",
                         f"{e}")


if __name__ == "__main__":
    main()
