import setuptools

if __name__ == "__main__":

    setuptools.setup(
        packages=setuptools.find_packages(),
        entry_points = {
        'console_scripts': [
            'nika_rss_reader = rss_reader.__main__:main'
        ]
    }
    )
